- Intro: Lập trình và tại sao lại cần có nó
- Bài 1: Máy tính và cách cài đặt phần mềm cần thiết
- Bài 2: Hello World! ABC với lập trình
- Bài 3: Biến và liên hệ với toán học
- Bài 4: Kiểu dữ liệu và phép tính 
- Bài 5: Class và mối liên hệ giữa thế giới thực và ảo
- Bài 6: Tìm kiếm trên internet như thế nào
- Bài 7: Vòng lặp
- Bài 8: Hàm 
- Bài 9: Trò chơi đầu tiên: Cái cân dạo
- Bài 10: Phần mềm đầu tiên: Từ điển xì tin


# Lập trình vui

- ## Bài Intro: 
    - Tiêu đề: 
        - Ý tưởng "Lập trình với Avenger" : Nhân vật người sắt
        - Ý tưởng "Lập trình vui" : 
    - Nội dung:
        - Giới thiệu về lập trình
            - Lập trình giúp tạo dựng `mọi thứ` trong cuộc sống vào máy tính
            - Lập trình khiến `Máy tính giúp con người tính toán` trước khi đưa ra quyết định đúng
            - Lập trình giúp con người `làm những việc không thể`
            - Môn học lập trình rất hữu ích và có tính ứng dụng cao
            - Dễ học, dễ nhớ
            - Chuẩn bị phần mềm đơn giản

- ## Bài 1: Máy tính và cách cài đặt phần mềm cần thiết: 
    - Tiêu đề: 
        - Ý tưởng "Lập trình với Avenger" : Utron học Python
        - Ý tưởng "Lập trình vui" : 
    - Nội dung:
        - Giới thiệu về máy tính
            - Hệ điều hành
            - Thư mục
            - Tệp, tệp cài đặt
        - Hướng dẫn tải và cài đặt phần mềm
            - Tải phần mềm python 3
            - Tải phần mềm Visual Studio Code
            - Tải các bổ trợ nhắc lệnh
        - Hướng dẫn cách gõ bàn phím
            - Phím phía trên
            - Phím phía dưới ấn kèm nút `shift`
            - Viết hoa: Giữ `shift`
            - Nút cách `space` và nút `enter`

- ## Bài 2: Hello World! ABC với lập trình
    - Tiêu đề:
        - Ý tưởng "Lập trình với Avenger" : Xin chào, tôi là Người sắt
        - Ý tưởng "Lập trình vui" : 
    - Nội dung:
        - Máy tính chỉ biết `chạy theo lệnh`
        - Giới thiệu về `trình tự chạy`: `Chạy từ trên xuống dưới`
        - Câu lệnh `print('Xin chào, tôi là người sắt')`
        - Câu lệnh `sleep` để tạm dừng chương trình
        - Dấu kết thúc câu lệnh `;`
        - Khởi chạy chương trình bằng nút `run`
        - Liên hệ câu lệnh lập trình với tiếng Anh
        - Xem chương trình được chạy trong `terminal`
        - Thực hành cái gì đó vui vui: `giới thiệu về bản thân`
- ## Bài 3: Biến và liên hệ toán học
    - Tiêu đề
        - Ý tưởng "Lập trình với Avenger" : Người sắt và chuyện cất áo giáp
        - Ý tưởng "Lập trình vui" : 
    - Nội dung
        - `Biến (variable)` là một đại diện cho một vùng `bộ nhớ` trong bộ nhớ máy tính, được `đặt tên` và `chứa` một `giá trị` nào đó
        - `Áo giáp` của người sắt được cất trong các `tủ (biến)` trong `phòng trưng bày (Bộ nhớ máy tính)`, được `đặt tên` và `chứa` bộ `giáp (giá trị)` tương ứng.
        - `Biến` sau khi được tạo ra có thể `gọi ra` để sử dụng<br>
         > `var greenSuit = "Giáp màu xanh"`<br>
         > `print(greenSuit)`;<br>
         > *Giống với*<br>
         > `print("Giáp màu xanh")`
        - Trong đó
            - `var`: Cấu trúc `khai báo biến` ~ nói với máy tính rằng `tôi muốn một vùng bộ nhớ`
            - `greenBody`: `Tên` của biến, ~ nói với máy rằng `tôi muốn biến này tên là greenSuit`
            - `=`: Phép `Gán giá trị` cho biến ~ nói với máy rằng `tôi muốn biến này có giá trị là`
            - `"Giáp màu xanh"`: Giá trị được gán cho biến 
        - Các cách đặt tên biến: 
            - CameoCase (con lạc đà): Chữ cái đầu viết thường, các chữ sau viết hoa và viết liền không dấu: `greenSuit`, `blueSuit`, `hulkBuster`
            - pythonCase: các chữ cái viết thường, nhưng được tách nhau bằng dấu `_` 
        - Ví dụ thực hành
- ## Bài 4: Dữ liệu, Kiểu dữ liệu và toán tử
    - Tiêu đề
        - Ý tưởng "Lập trình với Avenger" : Chế tạo áo giáp với `Kiểu dữ liệu` và `Toán tử`
        - Ý tưởng "Lập trình vui" : 
    - Nội dung
        - Kiểu dữ liệu:
            - `Kiểu dữ liệu` là các `loại` dữ liệu khác nhau, giống như `áo giáp` có thể làm từ `các vật liệu` khác nhau
            - `Kiểu dữ liệu` được chia ra làm `4` phân loại cơ bản chính: 
                - `Dạng chữ (chuỗi - string)` 
                - `Dạng số (số - number)` 
                    - Số nguyên: `int`
                    - Số thập phân: `float`, `double`, `decimal`
                - `Dạng đúng sai (boolean)`
                    - Đúng: `true`
                    - Sai: `false`
                - `Dạng NULL` : Không có gì
            - Ngoài ra còn có kiểu `thứ tự` (`stack - queue`), kiểu `danh sách` (`list`, `dictionary`, `tuple`, `set`) sẽ giới thiệu trong tương lai
        - Toán tử
            - Giống như toán, máy tính cũng có các phép tính cộng trừ nhân chia so sánh
            - Các dấu trong phép tính được gọi là `toán tử`, các `dữ liệu` hoặc `biến` được gọi là các `toán hạng`
            - Các dữ liệu hoặc biến có thể thực hiện `tính toán`, `so sánh`, `logic` với nhau nhưng chỉ khi chúng có cùng `kiểu dữ liệu`
            - Các toán tử  thường dùng:
                - Toán tử tính toán: Máy sẽ trả về `kiểu dữ liệu` tương ứng
                    > `+` : Cộng hai toán hạng 
                    >   + Nếu là `chuỗi - string` thì sẽ là `nối chuỗi`. vd: `"iron" + "man" = "ironman"`
                    >   + Nếu lá `số - number` thì sẽ tính toán như số bình thường </p>

                    > `-` : Trừ hai toán hạng (Chỉ áp dụng với các kiểu dữ liệu `số - nummber`)<br>
                    > `*` : Nhân hai toán hạng (Chỉ áp dụng với các kiểu dữ liệu `số - nummber`) <br>
                    > `/` : Chia hai toán hạng (Chỉ áp dụng với các kiểu dữ liệu `số - nummber`)<br>
                    > `%` : Lấy số dư của phép chia hai toán hạng (Chỉ áp dụng với các kiểu dữ liệu `số - nummber`)<br>
                    > `++` : Cộng thêm 1 đơn vị (Chỉ áp dụng với các kiểu dữ liệu `số - nummber`)<br>
                    > `--` : Trừ đi 1 đơn vị (Chỉ áp dụng với các kiểu dữ liệu `số - nummber`)<br>
                - Toán tử `so sánh - quan hệ`: Máy sẽ trả về `kiểu dữ liệu` là `đúng sai - boolean`
                    > `==` : So sánh hai giá trị có `giống` nhau hay không <br>
                    > `!=` : So sánh hai giá trị có `khác` nhau hay không <br>
                    > `>`  : So sánh giá trị bên trái có `lớn hơn` giá trị bên phải hay không <br>
                    > `<`  : So sánh giá trị bên trái có `nhỏ hơn` giá trị bên phải hay không <br>
                    > `>=` : So sánh giá trị bên trái có `lớn hơn hoặc bằng` giá trị bên phải hay không <br>
                    > `<=` : So sánh giá trị bên trái có `nhỏ hơn hoặc bằng` giá trị bên phải hay không <br>
                - Toán tử `logic`: Máy sẽ trả về `kiểu dữ liệu` là `đúng sai - boolean`
                    > `&&` : Được gọi là toán tử `AND`, nghĩa là `VÀ`<br>
                    > vd: `(1 == 1) && (2 == 2)` ~ `true AND true` => `true` </p>
                    > `||` : Được gọi là toán tử `OR`, nghĩa là `HOẶC` <br>
                    > vd: `(1 == 1) || (2 == 2)` ~ `true OR true` => `true` </p>
                    > `!` : Được gọi là toán tử `NOT`, nghĩa là `KHÔNG (Phủ định)` <br>
                    > vd: `!(1 == 1)` ~ `NOT true` => `false` </p>
        - Ví dụ bài tập

- ## Bài 5: Class và mối liên hệ giữa thực và ảo
    - Tiêu đề:
        - Ý tưởng "Lập trình với Avenger" : Đội trưởng Mĩ: Hồ sơ Avenger với `Class`
        - Ý tưởng "Lập trình vui" : 
    - Nội dung:
        - Đội trưởng Mỹ cần tạo hồ sơ với các thành viên trong đội bao gồm:
            - Tên : kiểu `chuỗi - string`
            - Tuổi: kiểu `số - number`
            - Vũ khí: kiểu `chuỗi - string`
            - Phép thuật: kiểu `boolean` (có phép thuật hoặc không có phép thuật)
        - Đội trưởng Mỹ lập ra một bảng như sau ![class example](https://i.ibb.co/HpM8vNR/ttt.png)
        - Dễ dàng nhận ra: Tất cả thành viên Averger đều có các thông tin về `Tên`, `Tuổi`, `Vũ khí`, `Phép thuật`. Tuy nhiên `Dữ liệu` của họ lại khác nhau, chỉ giống nhau về `Kiểu dữ liệu`
        - Thật may, ngôn ngữ lập trình cung cấp 1 khái niệm gọi là `Lớp - class` để Caption America có thể dễ dàng xây dựng hồ sơ cho đội Avenger, từ đó có thể biết được thông tin của người cần tìm.
        - (Hướng dẫn khai báo cấu trúc `class`)
            > Trong cuộc sống, vạn vật cũng được `quản lý` bằng các `thông tin - Trường dữ liệu (Property)` giống như đội Avenger, và khi tập hợp tất cả các thông tin trên, chúng ta có thể quản lý được `Dữ liệu` tổng thể và sử dụng để tra cứu, tính toán. Ví dụ: Lấy ra các Avenger nhiều hơn 100 tuổi
        - `Lập trình` chính là công việc `Mô phỏng` lại cuộc sống thật vào máy tính