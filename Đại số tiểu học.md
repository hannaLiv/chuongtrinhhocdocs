# Đại số tiểu học

- ## Bài Intro: 
    - Tiêu đề: 
        - Ý tưởng "Toán học vòng quanh thế giới" :Toán học và người Maya 
        - Ý tưởng "Học toán vui" : Toán học và cuộc sống
    - Nội dung:
        - Giới thiệu về toán học
            - Toán học để làm gì
            - Toán học được cấu thành từ gì
            - Lịch sử ra đời
            - Sự phát triển
- ## Bài 1: Chữ số và số
    - Tiêu đề: 
        - Ý tưởng "Toán học vòng quanh thế giới": Người Ai Cập và số đếm
        - Ý tưởng "Học toán vui": Số và dãy số
    - Nội dung:
        - Giới thiệu về số:
            - Một `số` là một đối tượng toán học được sử dụng để đếm, đo lường và dán nhãn [wiki](https://vi.wikipedia.org/wiki/S%E1%BB%91)
            - Một `số` được tạo ra từ các `Chữ số`
            - Các hàng: hàng đơn vị, hàng chục, hàng trăm...
            - Số là nền tảng của toán học và cuộc sống
- ## Bài 2: Số 0 và tia số
    - Tiêu đề:
        - Ý tưởng "Toán học vòng quanh thế giới": Người Anh (John Wallis) và tia số
        - Ý tưởng "Học toán vui" : Số 0, tia số và sự cân bằng hoàn hảo
    - Nội dung
        - Giới thiệu về tia số
            - Tia số và cái thước kẻ
            - Tia số tăng đến vô cùng từ âm đến dương
            - Số càng đi về phía bên phải thì càng `lớn`
            - Dựa vào tia số có thể xác định được `số lớn hơn` và `số bé hơn`
        - Kể chuyện về số 0 (Nội dung này chỉ nên là một câu chuyện ngắn)
            - Vị trí của số 0 trên tia số
            - Liên hệ tia số với chiếc cân, số 0 chính là điểm cân bằng
            - Tầm quan trọng của số 0
- ## Bài 3: Phép cộng và phép trừ
    - Tiêu đề:
        - Ý tưởng "Toán học vòng quanh thế giới": 
        - Ý tưởng "Học toán vui" : Phép cộng và phép trừ
    - Nội dung
        - `Phép tính` là 
        - Giới thiệu về các phép tính
            - Phép cộng 
                - Phép cộng và ví dụ về việc `Cộng thêm`, kết quả là `Tổng`
                - Thể hiện trên tia số 
                - Tính chất của phép cộng
                - Cách cộng hai số
            - Phép trừ 
                - Phép cộng và ví dụ về việc `Bớt đi`, `Số bị trừ`, `Số trừ` và `Hiệu`
                - Thể hiện trên tia số
                - Tính chất của phép trừ
                - Cách trừ hai số
- ## Bài 4: Phép nhân và bảng cửu chương
    - Tiêu đề:
        - Ý tưởng "Toán học vòng quanh thế giới": 
        - Ý tưởng "Học toán vui" : Phép nhân và dấu X quyền lực
    - Nội dung:
        - Giới thiệu về phép nhân
            - `Phép nhân` và sự rút gọn của `phép cộng`
            - `Nhân tử` và `Tích (Kết quả)`
            - Tính chất của phép nhân
            - Cách nhân hai số
            - Cách nhân nhanh
        - Giới thiệu về bảng cửu chương
            - Bảng cửu chương là 
            - Cách học thuộc bảng cửu chương thật nhanh!
- ## Bài 5: Phép chia và phân số
    - Tiêu đề: 
        - Ý tưởng "Toán học vòng quanh thế giới": 
        - Ý tưởng "Học toán vui" : Phép chia và cái bánh Pizza
    - Nội dung
        - Giới thiệu về phép chia
            - `Phép chia` và chiếc bánh Pizza bị cắt đều
            - `Số chia`, `Số bị chia` và `Thương` và `Số dư`
            - Tính chất của phép chia
            - Cách chia hai số
        - Giới thiệu về `Phân số`, `Số dư`
            - `Phân số` chính là phép chia với `Tử số` là `Số chia` và `mẫu số` là `Số bị chia`
            - So sánh 2 `Phân số`
            - Cân bằng phân số
            - Cộng trừ nhân chia `phân số`
            - Kết quả của phân số dược biểu diễn bởi số và dấu `thập phân` hay còn gọi là `Số hữu tỉ`
            - Ứng dụng của `Phân số` và `Số hữu tỉ`: `Làm tròn số` và `Phần trăm`
- ## Bài 6: Phép tính, dấu ngoặc và dấu bằng
    - Tiêu đề: 
        - Ý tưởng "Toán học vòng quanh thế giới": 
        - Ý tưởng "Học toán vui" : Phép tính, dấu ngoặc và dấu bằng
    - Nội dung:
        - Giới thiệu về phép tính
            - Phép tính là `sự thể hiện trên giấy`, gồm 2 hay nhiều `Toán hạng` được `Tính toán` cộng trừ nhân chia với nhau
            - `Phép tính` tạo thành từ `đầu vào` và có `kết quả` được ngăn cách bởi dấu `bằng`. Hai bên được gọi là `vế`
            - Dấu ngoặc và sự ưu tiên của phép tính
            - `Dấu bằng` và sự thay đổi của phép tính khi đi qua `dấu bằng`
            - (Giới thiệu) Để cho `phép tính` đỡ dài, người ta sử dụng nhiều các `ký hiệu toán học`
- ## Bài 7: Ẩn số và toán đố
    - Tiêu đề:
        - Ý tưởng "Toán học vòng quanh thế giới": 
        - Ý tưởng "Học toán vui" : Ẩn số và toán đố
    - Nội dung: 
        - Ẩn số là các `số cần tìm` trong một bài toán
        - Ẩn số và kí hiệu ẩn số thường dùng là `x` (_một kí hiệu toán học_)
        - Đặt phép tính với `ẩn số` được sử dụng kí hiệu và các `tham số` đã được biết
        - Tìm `ẩn số` bằng cách đưa ẩn số về một `vế` của phép tính, và kết quả ở `vế` còn lại
        - Ví dụ thực tế
- ## Bài 8: Phương trình thật tinh tường
    - Tiêu đề:
        - Ý tưởng "Toán học vòng quanh thế giới": 
        - Ý tưởng "Học toán vui" : Phương trình thật tinh tường
    - Nội dung:
        - `Phương trình` chính là `Phép tính` được biểu diễn cùng với kết quả `bằng`
        - `Phương trình` thường bao gồm các giá trị `ẩn số`,`phép toán`,`tham số` và `kết quả`
        - Việc tìm giá trị của `ẩn số` được gọi là `Giải phương trình`. Kết quả được gọi là `Nghiệm` 
        - Ở bậc tiểu học dừng lại ở `Phương trình bậc 1 có 1 ẩn số`
- ## Bài 9: Bài toán nào cũng là một `Phương trình`
    - Tiêu đề:
        - Ý tưởng "Toán học vòng quanh thế giới": 
        - Ý tưởng "Học toán vui" : Phương trình thật tinh tường
    - Nội dung
        - Bài toán thường bao gồm các `ẩn số`
        - Giải bài toán bằng cách:
            1. Xác định các `Ẩn số`, `Tham số` và mối liên quan
            2. Xây dựng các `Phép tính`
            3. Giải `Phép tính` để tìm `Ẩn só`
            4. Từ `Ẩn số` tìm ra các `Kết quả`
- ## Bài 10: Giới thiệu về thống kê
    - Tiêu đề:
        - Ý tưởng "Toán học vòng quanh thế giới":
        - Ý tưởng "Học toán vui": Giới thiệu về thống kê
    - Nội dung:
        - Ví dụ về `danh sách học sinh`, cô giáo muốn biết lớp học có `bao nhiêu bạn Nam`, `Bao nhiêu bạn nữ`
            - Cô giáo ghi lại `danh sách lớp` được gọi là `thu thập thông tin`
            - Cô giáo `đánh dấu bạn Nam màu đỏ`, `Đánh dấu bạn nữ màu xanh` được gọi là `Phân loại`
            - Cô giáo `đếm số chấm đỏ` để tìm tổng số bạn nam, `đếm số màu xanh` để tìm bạn nữ được gọi là `kiểm đếm` hay `thống kê`
            - Hiệu trưởng dựa vào bảng `thống kê` để biết được `lớp học nào nhiều bạn Nam` và `lớp học nào nhiều bạn Nữ` cho ngày hội của trường gọi là `trả lời câu hỏi`
        - Sự quan trọng của `thống kê` trong cuộc sống.





        

    

            

